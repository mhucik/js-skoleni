#Spuštění:
 - php -S 0.0.0.0:8765 -t www
 
 //inicializace package.json
 - npm init
 
 //instalace naja knihovny (místo nette.ajax)
 - npm install naja --save
 
 //doinstalace webpack
 - npm install --save-dev webpack
 
 //webpack pro cli
 - npm install --save-dev-webpack-cli

 - vytvořím si webpack.config.js (například podle vzoru https://webpack.js.org/guides/getting-started/#using-a-configuration)
 - nastavím sekci `scripts` v `package.json`, například takto:
 ```json
{
	"scripts": {
		"build": "webpack --config www-src/webpack.config.js --mode production"
	}
}
```
 - spustit `npm run build`
 
# Babel
https://babeljs.io/docs/en/usage
 - spustit:
 ```
npm install --save-dev @babel/core @babel/cli @babel/preset-env
npm install --save @babel/polyfill
npm install --save-dev babel-loader
```

 - řekne webpacku (do souboru `webpack.config.js`), aby načítal js soubory přes babel
 ```js
module.exports = {
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
			}
		]
	}
};
```

- vytvořit soubor `.babelrc` v rootu webu
```json
{
	"presets": [
		["@babel/env", 
			{
				"useBuiltIns": "usage"//kámoš babel polyfill; pollifiluje to promisy, mapset, datatype, příznak `usage` pak použije jenom ty věci, které jsou v kódu skutečně použity
			}
		]
	]
}
```

- vytvořit soubor `.browserslistsrc` v rootu webu
	- jde použít i přímo v `.babelrc`, ale lepší je to udělat odděleně, jelikož tento soubor mohou používat i nějaké další věci
	- `npx browserslist`


#Styly přes webpack
- instalace lessu
```
npm install --save-dev less less-loader css-loader style-loader mini-css-extract-plugin
```
konfigurace ve `webpack.config.js`:
```js
module.exports = {
	module: {
		rules: [
				{
					test: /\.less$/,
					use: [
						isProduction ? MiniCssExtractPlugin.loader : 'style-loader',//výsledné css pomocí javascriptu vloží do stránky, abych mohl snadněji vyvíjet css (na produkci, místo něho vygenerovaný minifikovaný css)
						'css-loader',//import nebo include prožene přes webpack, tzn například pokud mám načítání obrázků v background, tak to nastaví správnou cestu, případně obrázek přesune do dist složky
						'less-loader',//kompiluje less do css
					],
				},
				{
					test: /\.jpe?g$/,
					loader: 'file-loader',
				}
			]
		},
		plugins: [
			new MiniCssExtractPlugin({
				filename: "[name].css",
			})
		],
}
```

- pro autoprefixování použijeme `npm install --save-dev postcss-loader autoprefixer`
- pak založit postcss.config.js
```js
module.exports = {
	plugins: {
		autoprefixer: {},
	}
};
```

#WebDevServer 
pro autoloading, watch apod.
- `npm install --save-dev webpack-dev-server`
- do `webpack.config.js` doplnit 
```
"dev": "webpack-dev-server --hot --config webpack.config.js --mode development"
```
- spustit přes `npm run dev`

#Poznámky:
 - https://webpack.js.org/guides/getting-started/
 - na npm nemusím být stejné zdrojáky jako na githubu v projektu
 - není vhodné mít js soubory webpacku ve www složce, ale dát to o úroveň níž, aby to nebylo vidět na webu
 - https://naja.js.org/#/snippets (pokud chci navázat nějakou akci, například na dokončení zpracování snippetu, tak to je fajn použít)
 - pozor na loadery (v module->rules->use), je potřeba je dávat ve správném pořadí
 - balíček `oops/webpack-nette-adapter` pomůže při nastavování šablon a importů css/js v latte (https://github.com/o2ps/WebpackNetteAdapter)
