class TodoList {
	constructor(todo) {
		this.items = [];

		this.input = todo.querySelector('.js-todo-list__inp');
		this.form  = todo.querySelector('.js-todo-list__form');
		this.list  = todo.querySelector('.js-todo-list__list');

		this.form.addEventListener('submit', this.addTodo.bind(this));
		this.render();
	}


	addTodo(e) {
		e.preventDefault();

		var name = this.input.value;

		if (name.length) {
			let todo = {
				name: name,
				done: false
			};

			this.items.push(todo);
			this.input.value = '';

			this.renderTodo(todo);
		}
	};

	render() {
		for (let i = 0; i < this.items.length; i++) {
			this.renderTodo(this.items[i]);
		}
	}

	redraw() {
		this.list.innerHTML = '';

		this.render();
	}

	renderTodo(todo) {
		var _this = this;

		// Rodičovský element
		var todoEl = document.createElement('p');
		todoEl.className = 'js-todo-list__item item' + (todo.done ? ' item--done' : '');

		// Label, checkbox + name
		var todoLabelEl = document.createElement('label');
		todoLabelEl.className = 'item__label';

		var todoCheckboxEl = document.createElement('input');
		todoCheckboxEl.className = 'item__checkbox';
		todoCheckboxEl.type = 'checkbox';
		todoCheckboxEl.checked = todo.done;
		todoCheckboxEl.addEventListener('change', function (e) {
			e.preventDefault();
			_this.toggleTodo.call(_this, todo);
		});

		var todoTextEl = document.createElement(todo.done ? 'del' : 'span');
		todoTextEl.className = 'item__name';
		todoTextEl.innerText = todo.name;

		// Křížek
		var todoRemoveEl = document.createElement('button');
		todoRemoveEl.innerText = '×';
		todoRemoveEl.addEventListener('click', function (e) {
			e.preventDefault();
			_this.removeTodo.call(_this, todo);
		});

		// Všechno dohromady
		todoLabelEl.appendChild(todoCheckboxEl);
		todoLabelEl.appendChild(todoTextEl);

		todoEl.appendChild(todoLabelEl);
		todoEl.appendChild(todoRemoveEl);

		// Nejvíc nejhorší výkonostně...
		this.list.appendChild(todoEl);
	}


	toggleTodo(todo) {
		todo.done = !todo.done;

		this.redraw();
	};

	removeTodo(todo) {
		var i = this.items.indexOf(todo);

		this.items.splice(i, 1);

		this.redraw();
	}
}

