2.lekce
-
- uvnitř funkcí používat raději `currentTarget`, než `this`. CurrentTarget totiž vždy vrátí element, na kterém je navěšený event. (`eventTarget` se vytahuje se z události)
- classlist 
- preventDefault 
	- například neodešle formulář
- polyfil - pro nepodporovaný prohlížeč automaticky doplní určitou funkčnost, aniž by programátor musel něco dělat
- template literal - jiný způsob, jak zapsat řetězec, alternativa dvojitých uvozovek z PHP
##### arrow function
místo
```js
element.addEventListener('asdf', function (e) {
	//něco
})
```

jde dát

```js
element.addEventListener('asdf', (e) => {
	//něco
})
```

přebírá `this` z nadřazeného kontextu
##### IIFE
- Immediately-invoked function expression
syntaxe:
```js
(function () {
	//obsah funkce
})();
```

cokoliv, co definuju uvnitř této funkce, bude dostupné pouze uvnitř této funkce

jde taky dát 
```js
{
	//obsah funkce
}
``` 
##### LET vs CONST
```js

//funguje:
let a = 1;
a = 2;

//nefuguje:
const a = 1;
a = 2;

//funguje
const a = [];
a.push('1');
```
- obojí je dostupné pouze ve scope, narozdíl od `var`
- můžu ale modifikovat obsah, například pole (nesmí tam být přířazení)

```js
if (1 == 1) {
	const a = 1;
	//tady je konstanta `a` dostupná
}

//tady už konstanta `a` dostupná není
```


##### Prototype
- https://hackernoon.com/prototypes-in-javascript-5bba2990e04b

##### Bind
- myslím, že trochu souvisí s IIFE. Používá se kvůli tomu, abych měl `this` stejné, jako v tom nabindovaném elementu 

##### Local storage
- můžu ukládat data do lokální paměti prohlížeče
- je dostupný hned ve window
- není to moc spolehlivé úložiště

##### vyhazování výjimek
```js
function foo() {
	throw new Error();
}

try {
	foo();
} catch (error) {
	
} finally {
	
}
```
